/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse ;
//import javax.servlet.http.HttpSession ;
import javax.servlet.ServletContext ;
import java.io.*;
import java.util.*;
import mtngc.sms.SMSClient;
import mtngc.ussd.AbstractBundleSession;
import mtngc.ussd.AbstractMainMenu;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import mtngc.ussd.AbstractUSSDHandler;
import mtngc.ussd.AbstractBundleSession;
import mtngc.ussd.segmentedCombo.data.*;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

import org.apache.commons.lang.StringUtils;
/**
 *
 * @author ekhosa
 */
public class SEGCOMBOUSSDHandler {
    
    static String ContextKeyName;
    static ArrayList <Integer> supportedMSISDNs;
    String FreeFlow = null;
    
    static
    {  
        ContextKeyName = "ComJAIMOverUCIP";
        String filePath = "C:\\Logs\\"+ContextKeyName+"_v1";
        try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            }else{
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        MLogger.setHomeDirectory(filePath);
    }
    
    public void handleRequest(HttpServletRequest request, HttpServletResponse response){
        TraceParametersAndHeaders(request);
            
        try {
            String msisdnStr = request.getParameter("MSISDN");
            //msisdnStr = "224660134178";
            int msisdn = 0;
            try{msisdnStr = msisdnStr.substring(3);}catch(Exception n){};
            try{msisdn = Integer.parseInt(msisdnStr);}catch(Exception n){};
            
            if(IsMSISDNAllowed(msisdn)){
                processRequest(msisdnStr,request, response);
            }else{
                response.setContentType("UTF-8");
                response.setHeader("Freeflow", "FB");
                response.setHeader("cpRefId", "emk2545");
                PrintWriter out = response.getWriter();
                out.append("Le service n'est pas disponible");           
                out.close();            
            }
            
        } catch (Exception e) {
            MLogger.Log(this, e);
        }    
    }
    
    public void processRequest(String msisdn,HttpServletRequest request, HttpServletResponse response){
        this.FreeFlow = "FC";
        try {
            
            String ussdSessionid = request.getParameter("SESSIONID");            
            String input = request.getParameter("INPUT");

            ServletContext context = request.getServletContext();

            StringBuilder sb = new StringBuilder();
                
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"| Processing request.");
                            
                String contextKey = GetContextKey(ussdSessionid);
                Object obj = context.getAttribute(contextKey);
                if(obj == null){
                    if(input != null){
                        String str = displayMainMenu(context, ussdSessionid, msisdn);
                        sb.append(str );
                    }else
                        sb.append("Application is running");
                }else {
                    
                    BundleSession bundleSession = (BundleSession) obj;
                    int step = bundleSession.getStep();
                    switch (step) {
                        case 1:
                            {
                                // if previous screen was the main menu
                                String str = processMainMenuInput(input,  msisdn,  ussdSessionid, bundleSession, context);
                                sb.append(str);
                                break;
                            }
                        case 2:
                            {
                                // if previous screen was the data bundle menu (bundle name) menu
                                String str = processConfirmMenuInput(input,  msisdn,  ussdSessionid, bundleSession, context);
                                sb.append(str);
                                break;
                            }
                        default:
                            {
                                // if previous step is not supported
                            String str = displayMainMenu(context, ussdSessionid, msisdn);
                            sb.append(str);
                            break;
                        }
                    }
                }
                
            //}
            response.setContentType("UTF-8");
            response.setHeader("Freeflow", FreeFlow);
            response.setHeader("cpRefId", "emk2545");
            //response.setContentType(type);
            PrintWriter out = response.getWriter();
//            out.append("Your string goes here\n");
//            out.append("Your string goes here");
            
            out.append(sb.toString());
            out.close();
        } catch (Exception e) {
            MLogger.Log(this, e);
        }
    }
    protected String displayMainMenu(ServletContext context, String ussdSessionid, String msisdn){
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list");
        BundleSession bundleSession = new BundleSession();
        String contextKey = GetContextKey(ussdSessionid);
        Object obj = context.getAttribute(contextKey);
        
        if(obj != null){
            context.removeAttribute(contextKey);
        }
        bundleSession.setUssdSessionId(ussdSessionid);
        bundleSession.setStep(1);
        context.setAttribute(contextKey, bundleSession);
        MainMenu mainMenu = new MainMenu(msisdn);
        String str = mainMenu.getString();
//        if(!mainMenu.isAllowed()){
//            this.FreeFlow = "FB";
//        }
        
        return str;
    }
    
    public String processMainMenuInput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession,ServletContext context){
        String msg = null;
        
        if((input != null) && ( !input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim())) ){   
            input =  input.trim();
            RequestEnum requestEnum = null;
            
            StringBuilder sb = new StringBuilder();
            ComboData dataCombo = new ComboData();
            List<ComboMsisdn> list = dataCombo.getComboMsisdn(msisdn);
            ComboMsisdn comboMsisdn = null;
            int i=1;
            for(ComboMsisdn bundleMsisn : list){
                if(input.equals(i+"")){
                   comboMsisdn = bundleMsisn;
                }
                i++;
            }
            if(input.equals("1")){
                requestEnum = RequestEnum.REQ1;
                if(comboMsisdn != null){
                    sb.append("\n");
                    sb.append("Confirmez vous l achat du forfait de "+comboMsisdn.getBenefit()+"FG valide "+comboMsisdn.getValidity()+""+comboMsisdn.getValidity_type()+" a "+comboMsisdn.getPrice()+" GNF?\n");
                    sb.append("1. Confirmer\n");
                    sb.append("# Retour\n");
                    sb.append("Repondez");
                    bundleSession.setReq(comboMsisdn.getRefillProdId());
                    MLogger.Log(this, LogLevel.DEBUG, "Refill Profile IDReq1  ="+comboMsisdn.getRefillProdId());
                    bundleSession.setMsisdn(msisdn);
                    msg = sb.toString();
                }
            }else if(input.equals("2")){
                requestEnum = RequestEnum.REQ2; 
                if(comboMsisdn != null){
                    sb.append("\n");
                    sb.append("Confirmez vous l achat du forfait de "+comboMsisdn.getBenefit()+"FG valide "+comboMsisdn.getValidity()+""+comboMsisdn.getValidity_type()+" a "+comboMsisdn.getPrice()+" GNF?\n");
                    sb.append("1. Confirmer\n");
                    sb.append("# Retour\n");
                    sb.append("Repondez");
                    bundleSession.setReq(comboMsisdn.getRefillProdId());
                    MLogger.Log(this, LogLevel.DEBUG, "Refill Profile IDReq2  ="+comboMsisdn.getRefillProdId());
                    bundleSession.setMsisdn(msisdn);
                    msg = sb.toString();
                }
            }else if(input.equals("3")){
                requestEnum = RequestEnum.REQ3;
                if(comboMsisdn != null){
                    sb.append("\n");
                    sb.append("Confirmez vous l achat du forfait des "+comboMsisdn.getBenefit()+""+comboMsisdn.getUnite()+" valide "+comboMsisdn.getValidity()+""+comboMsisdn.getValidity_type()+" a "+comboMsisdn.getPrice()+" GNF?\n");
                    //sb.append("Confirmez-vous l’achat des "+comboMsisdn.getBenefit()+""+comboMsisdn.getUnite()+" a "+comboMsisdn.getPrice()+" GNF?\n");
                    sb.append("1. Confirmer\n");
                    sb.append("# Retour\n");
                    sb.append("Repondez");
                    bundleSession.setReq(comboMsisdn.getRefillProdId());
                    MLogger.Log(this, LogLevel.DEBUG, "Refill Profile IDReq3  ="+comboMsisdn.getRefillProdId());
                    bundleSession.setMsisdn(msisdn);
                    msg = sb.toString();
                }
            }
            else if(input.equals("#")){
                requestEnum = RequestEnum.RETOUR;
                //this.displayMainMenu(context, ussdSessionid, msisdn);
            }
            if(!input.equals("00")){
                bundleSession.setStep(2);
                bundleSession.setSelection(requestEnum);
                String contextKey = GetContextKey(ussdSessionid);
                context.setAttribute(contextKey, bundleSession);

            }
        }else{
            msg = displayMainMenu(context, ussdSessionid, msisdn);
        }    
        return msg;
    }
        
    public String processConfirmMenuInput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession,ServletContext context){
        String msg = null;
        this.FreeFlow = "FB";
        
        String comJaimAirTime = ContextKeyName;
        
        RequestEnum requestEnum = bundleSession.getSelection();
        
        MLogger.Log(this, LogLevel.DEBUG, "RequestEnum "+requestEnum);
        
        ComboData dataCombo = new ComboData();
        
        SMSClient smsClient = new SMSClient();
        

        if((input != null) && (!input.trim().equals("")) && (input.trim().length() >= 1) && (StringUtils.isNumeric(input.trim())) ){
            input =  input.trim();
            MLogger.Log(this, LogLevel.DEBUG, "Processing confirmation input "+input);
            //bundleSession.setCustmerMsisdn(input);
            String  comboRefillProdId = bundleSession.getReq();

            SegmentedrefillProdID selectedRefillProdId = dataCombo.getSegmentedrefillProdId(comboRefillProdId);
            
            MLogger.Log(this, LogLevel.DEBUG, "Refill Profile  ID="+selectedRefillProdId.getRefillProdId());

            if(input.equals("1")){
                if(selectedRefillProdId != null){
                    ProvisioningEngine provengin = new ProvisioningEngine();
                    ResponseEnum respEnum = provengin.execute(msisdn, ussdSessionid, selectedRefillProdId, comJaimAirTime);
                    if(respEnum == ResponseEnum.SUCCESS){
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|"+selectedRefillProdId.getRefillProdId()+"| Main Account.");
                        msg = "Votre souscription a MTN ComJAIM  est en cours de traitement. Vous recevrez un message de confirmation dans un instant.\n";
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|"+selectedRefillProdId.getRefillProdId()+"| Main Account.");
                    }else if(respEnum == ResponseEnum.ERROR){
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|"+selectedRefillProdId.getRefillProdId()+"| Main Account.");
                        msg = "Il y a erreur de souscription. Merci de ressayer.\n";
                    }else if(respEnum == ResponseEnum.NOT_ALLOWED){
                        msg = "Cher client, vous n etes pas autorise a utiliser ce service. Pour plus d informations, appelez le 111. Merci";
                    }else if(respEnum == ResponseEnum.BALANCEINSUFUSANT){
                        msg = "Votre solde est insuffisant pour souscrire a ce forfait. Veuillez recharger votre compte et ressayer.";
                    }else{
                        msg = "Il y'a erreur.\n";
                    }   
                }else{
                    msg = "Erreur Keyword!\n";
                }
            }else{
                msg = displayMainMenu(context, ussdSessionid, msisdn);
            }
            String contextKey = GetContextKey(ussdSessionid);
            context.removeAttribute(contextKey);
        }else{
            msg = displayMainMenu(context, ussdSessionid, msisdn);
        }
        return msg;
    }
    
    private void SendSuccessSMSA(String msisdn, String msg){

        String msisdnStr = msisdn;
        //msisdn = "664222545";
        if(!msisdnStr.startsWith("224")){
            msisdnStr = "224" + msisdnStr;
        } 
        
        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("ComJAIM", msisdnStr, msg);
        
    }
    
    private void SendSuccessSMSB(String msisdn, String msg){
        ComboData dataCombo = new ComboData();
        
       
        
        //String customerMsisdns = customersMsisdn.getCustomerMsisdn();

        String msisdnStr = msisdn;
        if(!msisdnStr.startsWith("224")){
            msisdnStr = "224" + msisdnStr;
        } 
        
        SMSClient smsClient = new SMSClient();
//       int msisdnInt = Integer.parseInt(msisdnStr);
//       SponsoreMsisdn sponsorMsisdn = dataCombo.getSegmentedComboSponsor(msisdnInt);
        smsClient.sendSMS("ComJAIM", msisdnStr, msg);
        
    }

    protected void TraceParametersAndHeaders(HttpServletRequest request){
        
        Enumeration<String> headerEnum = request.getHeaderNames();
        MLogger.Log(this, LogLevel.DEBUG, "Tracing HTTP Headers and Parameters ");
        
        while(headerEnum.hasMoreElements()) {
            String headerName = headerEnum.nextElement();
            String headerValue = request.getHeader(headerName);
            //System.out.println("Header:- "+headerName+": "+headerValue);
            MLogger.Log(this, LogLevel.DEBUG, "Header:- "+headerName+": "+headerValue);
        } 
        
        Map<String, String[]> map = request.getParameterMap();
        //Reading the Map
        //Works for GET && POST Method
        for(String paramName:map.keySet()) {
            String[] paramValues = map.get(paramName);

            //Get Values of Param Name
            for(String valueOfParam:paramValues) {
                //Output the Values
                //System.out.println("Value of Param with Name "+paramName+": "+valueOfParam);
                //ystem.out.println("Parameter:- "+paramName+": "+valueOfParam);
                MLogger.Log(this, LogLevel.DEBUG, "Parameter:- "+paramName+": "+valueOfParam);
            }
        }           
        
    }
    
    protected String GetContextKey(String ussdSessionid){
        if (ContextKeyName == null)
            throw new RuntimeException("ContextKeyName not set");
        return ContextKeyName+"-"+ussdSessionid;
    } 
    
    protected boolean IsMSISDNAllowed(int msisdn){
        if((supportedMSISDNs == null) || (supportedMSISDNs.size() == 0))
            return true;
        if(msisdn == 0)
            return true;
        for(int n : supportedMSISDNs){
            if(n==msisdn)
                return true;
        }
        
        return false;
    }
}
