/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo;
import mtngc.ussd.AbstractBundleSession;

/**
 *
 * @author EKhosa
 */
public class BundleSession  extends AbstractBundleSession {
    private String ussdSessionId;
    private String msisdn;
    private RequestEnum selection;
    private int step;
    private String req;
    private String custmerMsisdn;

    /**
     * @return the ussdSessionId
     */
    public String getUssdSessionId() {
        return ussdSessionId;
    }

    /**
     * @param ussdSessionId the ussdSessionId to set
     */
    public void setUssdSessionId(String ussdSessionId) {
        this.ussdSessionId = ussdSessionId;
    }

    /**
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the selection
     */
    public RequestEnum getSelection() {
        return selection;
    }

    /**
     * @param selection the selection to set
     */
    public void setSelection(RequestEnum selection) {
        this.selection = selection;
    }

    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * @return the req
     */
    public String getReq() {
        return req;
    }

    /**
     * @param req the req to set
     */
    public void setReq(String req) {
        this.req = req;
    }

    /**
     * @return the custmerMsisdn
     */
    public String getCustmerMsisdn() {
        return custmerMsisdn;
    }

    /**
     * @param custmerMsisdn the custmerMsisdn to set
     */
    public void setCustmerMsisdn(String custmerMsisdn) {
        this.custmerMsisdn = custmerMsisdn;
    }

    
}
