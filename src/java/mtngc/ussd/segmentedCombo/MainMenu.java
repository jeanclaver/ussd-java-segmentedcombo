/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo;
//import com.sun.org.apache.xml.internal.security.Init;
import java.io.Serializable;
import java.util.*;
//import dbaccess.dbsrver.*;
import mtngc.ussd.segmentedCombo.data.ComboBundle;
import mtngc.ussd.segmentedCombo.data.ComboData;
import mtngc.ussd.segmentedCombo.data.SegmentedMsisdn;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import mtngc.ussd.AbstractMainMenu;
import mtngc.ussd.segmentedCombo.data.ComboBundle22;
/**
 *
 * @author mpdiallo
 */
public class MainMenu {
    String header;
    String msisdnCombo;
    protected String msg;    
    public static HashMap SEGMENTED_MAIN_MENU = new HashMap();
    private final static Object _lock = new Object(); 
    
    public MainMenu(String msisdnCombo){
        this.msisdnCombo=msisdnCombo;
    }

    public String getString(){
        header = "MTNComJAIM (TOUT RESEAU)";
        StringBuilder sb = new StringBuilder(); 
        sb.append(header+"\n");
        ComboData dataCombo = new ComboData();
        List<ComboBundle> list = dataCombo.getBundles(msisdnCombo);
        int i=1;
        for(ComboBundle bundle : list){
            sb.append(i+". "+bundle.getPrice()+" GNF = "+bundle.getBenefit()+" "+bundle.getUnite()+"("+bundle.getValidity()+""+bundle.getValidity_type()+")\n");
            i++;
        }
        //sb.append("4. Invitez et gagnez 10min\n");
        sb.append("Repondez");
            
        String str = sb.toString();
        return str;
    }
        
}
