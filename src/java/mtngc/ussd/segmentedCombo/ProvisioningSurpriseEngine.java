/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import mtngc.ussd.segmentedCombo.data.*;
import ucipclient.UCIPClientEngine;
import ucipclient.UCIPGetAccountDetailsResponse;
import ucipclient.UCIPUpdateBalanceAndDateResponse;
import ucipclient.UCIPUpdateServiceClassResponse;
import ucipclient.UCIPUpdateRefillIDResponse;




/**
 *
 * @author mpdiallo
 */
public class ProvisioningSurpriseEngine {
    
    private static int[] supportedServiceClasses = {0, 1, 2, 3, 4, 6, 15, 30, 20, 28, 19, 102, 31, 40, 66, 103, 45};

    private String pourcentageAchat;

    ResponseEnum execute(String msisdn, String transactionId, double bundlePrice, String comJAIMUCIP){
        ResponseEnum respEnum  = ResponseEnum.SUCCESS;
        double pourcentage;
        double amountRefilled;
        
        
        
        String refillExternalData = "CJMSurprise";
        
        ComboData dataCombo = new ComboData();

        UCIPClientEngine ucipEngine = new UCIPClientEngine();
        UCIPGetAccountDetailsResponse getAccountResponse = ucipEngine.GetAccountDetails(msisdn, transactionId);
        //UCIPClientRefillProfileIdEngine refillProfileIdUcipEngine = new UCIPClientRefillProfileIdEngine();
        
        //UCIPGetAccountDetailsResponse2 getAccountResponse2 = ucipEngine2.GetAccountDetails(msisdn, transactionId);
        
        if(getAccountResponse != null){
            
            int sc = getAccountResponse.getServiceClass();
            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|Service Class="+sc);
            if(this.IsServiceClassAllowed(sc)){
                double balance = getAccountResponse.getBalance();                
                double price = bundlePrice;
                if(price > balance){
                  respEnum = ResponseEnum.BALANCEINSUFUSANT;
                }else{
                    
                    if(price >= 1 && price  == 1999){
                        pourcentageAchat = "400%";
                        pourcentage = 0.25;
                        amountRefilled = price * pourcentage;
                    }else if(price >= 2000 && price  == 4999){
                        pourcentageAchat = "400%";
                        pourcentage = 0.25;
                        amountRefilled = price * pourcentage;
                    }else if(price >= 5000 && price  == 9999){
                        pourcentageAchat = "500%";
                        pourcentage = 0.2;
                        amountRefilled = price * pourcentage;
                    }else if(price >= 10000){
                        pourcentageAchat = "500%";
                        pourcentage = 0.2;
                        amountRefilled = price * pourcentage;
                    }
                    MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|amount Price ="+price);
                    
                    UCIPUpdateBalanceAndDateResponse deductMAResponse = ucipEngine.UpdateMainAccountBalanceAndDate(msisdn, transactionId, -bundlePrice, true, new Date(), comJAIMUCIP);                    
                    int x = deductMAResponse.getResponseCode();
                    if((x == 0) && (!deductMAResponse.isError())){
                        java.util.Calendar cal =java.util.Calendar.getInstance();        
                        //cal.add(java.util.Calendar.DATE,comboBundle.getValidity()); // this will add validity days        
                        Date expiry =  cal.getTime();
                        
                        UCIPUpdateRefillIDResponse refillProfileIdResponse = ucipEngine.RefillProfileID(msisdn, transactionId, "M11", refillExternalData);
                        int y = refillProfileIdResponse.getResponseCode();        
                        if((y == 0) && (!refillProfileIdResponse.isError())){
                            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|externalData Response: "+comJAIMUCIP);
                            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|Refill Profil ResponseCode: "+refillProfileIdResponse.getResponseCode());
                            respEnum  = ResponseEnum.SUCCESS;
                            int msisdnInt = Integer.parseInt(msisdn); 
                            //dataCombo.insertTransaction(msisdn, "NULL", comboBundle.getSegment_Id(), price, comboBundle.getBenefit(), comboBundle.getValidity(), comboBundle.getRefillProdId(), transactionId, "SUCCESS", "NULL", comboBundle.getDaId());
                        }else{
                            UCIPUpdateBalanceAndDateResponse refillMAResponse = ucipEngine.UpdateMainAccountBalanceAndDate(msisdn, transactionId, +price, true, new Date(), comJAIMUCIP);
                            if((x == 0) && (!refillMAResponse.isError())){
                                respEnum  = ResponseEnum.ERROR;
                                //dataCombo.insertTransaction(msisdn, comboBundle.getSegName(), comboBundle.getSegment_Id(), price, comboBundle.getBenefit(), comboBundle.getValidity(), comboBundle.getRefillProdId(), transactionId, "ERROR", comboBundle.getUserOption(), comboBundle.getDaId());
                            }
                        }
                    }else{
                        respEnum  = ResponseEnum.ERROR;
                        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|error:"+deductMAResponse.getMessage());
                        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId);
                    }
                }
            }else{
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId+"|Service Class="+sc);
                respEnum  = ResponseEnum.NOT_ALLOWED;
            }
        }else{
            respEnum  = ResponseEnum.ERROR;
            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|error:"+getAccountResponse);
            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionId);
        }
        return respEnum;

    }
    private boolean IsServiceClassAllowed(int sc){
        for(int n : supportedServiceClasses){
            if(n==sc)
                return true;
        }
        
        return false;
    }
    
}
