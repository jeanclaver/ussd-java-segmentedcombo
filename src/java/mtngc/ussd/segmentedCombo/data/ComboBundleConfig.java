/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo.data;

import mtn.gn.com.data.combo.*;
import mtn.gn.com.data.*;
import java.io.Serializable;

/**
 *
 * @author EKhosa
 */
public class ComboBundleConfig implements Serializable {
    

    private int id;
    private String name;
    private String segment_Id;
    private int type;
    private int price;
    private int benefit;
    private String keyword;
    private String userOption;
    private String msisdnCombo;
    private String validity;
    private String segVoice;
    private String segData;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the segment_Id
     */
    public String getSegment_Id() {
        return segment_Id;
    }

    /**
     * @param segment_Id the segment_Id to set
     */
    public void setSegment_Id(String segment_Id) {
        this.segment_Id = segment_Id;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the benefit
     */
    public int getBenefit() {
        return benefit;
    }

    /**
     * @param benefit the benefit to set
     */
    public void setBenefit(int benefit) {
        this.benefit = benefit;
    }

    /**
     * @return the keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * @param keyword the keyword to set
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * @return the userOption
     */
    public String getUserOption() {
        return userOption;
    }

    /**
     * @param userOption the userOption to set
     */
    public void setUserOption(String userOption) {
        this.userOption = userOption;
    }

    /**
     * @return the msisdnCombo
     */
    public String getMsisdnCombo() {
        return msisdnCombo;
    }

    /**
     * @param msisdnCombo the msisdnCombo to set
     */
    public void setMsisdnCombo(String msisdnCombo) {
        this.msisdnCombo = msisdnCombo;
    }

    /**
     * @return the validity
     */
    public String getValidity() {
        return validity;
    }

    /**
     * @param validity the validity to set
     */
    public void setValidity(String validity) {
        this.validity = validity;
    }

    /**
     * @return the segVoice
     */
    public String getSegVoice() {
        return segVoice;
    }

    /**
     * @param segVoice the segVoice to set
     */
    public void setSegVoice(String segVoice) {
        this.segVoice = segVoice;
    }

    /**
     * @return the segData
     */
    public String getSegData() {
        return segData;
    }

    /**
     * @param segData the segData to set
     */
    public void setSegData(String segData) {
        this.segData = segData;
    }

   
}
