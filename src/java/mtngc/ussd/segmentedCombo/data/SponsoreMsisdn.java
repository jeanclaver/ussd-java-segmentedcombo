/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo.data;

import mtn.gn.com.data.combo.*;
import mtn.gn.com.data.*;
import java.io.Serializable;

/**
 *
 * @author EKhosa
 */
public class SponsoreMsisdn implements Serializable {
    
    private String msisdn;
    private int customerMsisdn;
    private int refill;
    private int validity;
    private int da;
    private String duration;

    /**
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the customerMsisdn
     */
    public int getCustomerMsisdn() {
        return customerMsisdn;
    }

    /**
     * @param customerMsisdn the customerMsisdn to set
     */
    public void setCustomerMsisdn(int customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
    }

    /**
     * @return the refill
     */
    public int getRefill() {
        return refill;
    }

    /**
     * @param refill the refill to set
     */
    public void setRefill(int refill) {
        this.refill = refill;
    }

    /**
     * @return the validity
     */
    public int getValidity() {
        return validity;
    }

    /**
     * @param validity the validity to set
     */
    public void setValidity(int validity) {
        this.validity = validity;
    }

    /**
     * @return the da
     */
    public int getDa() {
        return da;
    }

    /**
     * @param da the da to set
     */
    public void setDa(int da) {
        this.da = da;
    }

    /**
     * @return the duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    
}
