/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo.data;

import mtn.gn.com.data.combo.*;
import mtn.gn.com.data.*;
import java.io.Serializable;

/**
 *
 * @author EKhosa
 */
public class AlreadyExiste_Sponsor implements Serializable {

    private String customerMsisdn;
    private String bonusMsisdn;

    /**
     * @return the customerMsisdn
     */
    public String getCustomerMsisdn() {
        return customerMsisdn;
    }

    /**
     * @param customerMsisdn the customerMsisdn to set
     */
    public void setCustomerMsisdn(String customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
    }

    /**
     * @return the bonusMsisdn
     */
    public String getBonusMsisdn() {
        return bonusMsisdn;
    }

    /**
     * @param bonusMsisdn the bonusMsisdn to set
     */
    public void setBonusMsisdn(String bonusMsisdn) {
        this.bonusMsisdn = bonusMsisdn;
    }

}
