/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo.data;

import mtn.gn.com.data.combo.*;
import mtn.gn.com.data.*;
import java.io.Serializable;

/**
 *
 * @author EKhosa
 */
public class ComboMsisdn implements Serializable {

    private String keywordCombo;
    private String name;
    private String segment_Id;
    private int type;
    private int price;
    private int benefit;
    private int validity;
    private String userOption;
    private String unite;
    private String msisdn;
    private String segVoice;
    private String segData;
    private String validity_type;
    private String refillProdId;
    private int daId;

    /**
     * @return the keywordCombo
     */
    public String getKeywordCombo() {
        return keywordCombo;
    }

    /**
     * @param keywordCombo the keywordCombo to set
     */
    public void setKeywordCombo(String keywordCombo) {
        this.keywordCombo = keywordCombo;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the segment_Id
     */
    public String getSegment_Id() {
        return segment_Id;
    }

    /**
     * @param segment_Id the segment_Id to set
     */
    public void setSegment_Id(String segment_Id) {
        this.segment_Id = segment_Id;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the benefit
     */
    public int getBenefit() {
        return benefit;
    }

    /**
     * @param benefit the benefit to set
     */
    public void setBenefit(int benefit) {
        this.benefit = benefit;
    }

    /**
     * @return the validity
     */
    public int getValidity() {
        return validity;
    }

    /**
     * @param validity the validity to set
     */
    public void setValidity(int validity) {
        this.validity = validity;
    }

    /**
     * @return the userOption
     */
    public String getUserOption() {
        return userOption;
    }

    /**
     * @param userOption the userOption to set
     */
    public void setUserOption(String userOption) {
        this.userOption = userOption;
    }

    /**
     * @return the unite
     */
    public String getUnite() {
        return unite;
    }

    /**
     * @param unite the unite to set
     */
    public void setUnite(String unite) {
        this.unite = unite;
    }

    /**
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the segVoice
     */
    public String getSegVoice() {
        return segVoice;
    }

    /**
     * @param segVoice the segVoice to set
     */
    public void setSegVoice(String segVoice) {
        this.segVoice = segVoice;
    }

    /**
     * @return the segData
     */
    public String getSegData() {
        return segData;
    }

    /**
     * @param segData the segData to set
     */
    public void setSegData(String segData) {
        this.segData = segData;
    }

    /**
     * @return the validity_type
     */
    public String getValidity_type() {
        return validity_type;
    }

    /**
     * @param validity_type the validity_type to set
     */
    public void setValidity_type(String validity_type) {
        this.validity_type = validity_type;
    }

    /**
     * @return the refillProdId
     */
    public String getRefillProdId() {
        return refillProdId;
    }

    /**
     * @param refillProdId the refillProdId to set
     */
    public void setRefillProdId(String refillProdId) {
        this.refillProdId = refillProdId;
    }

    /**
     * @return the daId
     */
    public int getDaId() {
        return daId;
    }

    /**
     * @param daId the daId to set
     */
    public void setDaId(int daId) {
        this.daId = daId;
    }

    
}
