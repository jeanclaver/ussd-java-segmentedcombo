/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo.data;
//import dbaccess.dbsrver.AbstractDBSrverClient;
//import com.mtn.gn.configs.*;
import mtn.gn.com.data.*;
import dbaccess.dbsrver.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import java.util.*;
//import mtngc.ussd.segmentedCombo.AbstractDBSrverClient;
/**
 *
 * @author mpdiallo
 */
public class ComboData extends AbstractDBSrverClient {
    public ComboData() {
        
    }
    
    public int insertTransaction (String msisdn, String segmentName, String segmentId, int price, int benefit,int validity, String refillProId, String transactionId, String response, String optionId, int daId) {
               
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
 //select MSISDN, SEGMENTNAME, SEGMENT_ID, PRICE, BENEFIT, VALIDITY, REFILL_ID, TRANSACTION_ID, RESPONSE, OPTION_ID, DA_ID
            sb.append(" INSERT INTO  ");
            sb.append(" SEGMENTED_UCIP_TRANSAC ( ");
            sb.append(" MSISDN, SEGMENTNAME, SEGMENT_ID, PRICE, BENEFIT, VALIDITY, REFILL_ID, TRANSACTION_ID, RESPONSE, OPTION_ID, DA_ID)");
            sb.append(" VALUES (   ?,   ?,   ?,   ?,   ?,   ?,  ?,  ?,  ?,  ?, ?) ");                             
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1, msisdn);
            pstmt.setString(2, segmentName);
            pstmt.setString(3, segmentId);
            pstmt.setInt(4, price);
            pstmt.setInt(5, benefit);
            pstmt.setInt(6, validity);
            pstmt.setString(7, refillProId);
            pstmt.setString(8, transactionId);
            pstmt.setString(9, response);
            pstmt.setString(10, optionId);
            pstmt.setInt(11, daId);

            pstmt .executeUpdate();
           

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    

    public List<ComboMsisdn> getComboMsisdn(String msisdnCombo) {
        List<ComboMsisdn> list = new ArrayList<ComboMsisdn>();

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();

            sb.append("SELECT * "); 
            sb.append("FROM SEGMENTED_COMBO_OFFER_RID b, SEGMENTED_COMBO_MSISDN a "); 
            sb.append("where a.MSISDN = '"+msisdnCombo+"' and a.SEGMENT_ID = b.SEGMENT_ID ");
            sb.append("order by OPTION_ID asc ");
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                ComboMsisdn comboMsisdn = new ComboMsisdn();
                
                String keyword_Combo = rs.getString("KEYWORD");
                comboMsisdn.setKeywordCombo(keyword_Combo);
                int price = rs.getInt("PRICE");
                comboMsisdn.setPrice(price);
                int benefit = rs.getInt("BENEFIT");
                comboMsisdn.setBenefit(benefit);
                String msisdn = rs.getString("MSISDN");
                comboMsisdn.setMsisdn(msisdn);
                int validity = rs.getInt("VALIDITY");
                comboMsisdn.setValidity(validity);
                String unite = rs.getString("UNITE");
                comboMsisdn.setUnite(unite);
                String validity_type = rs.getString("VALIDITY_TYPE");
                comboMsisdn.setValidity_type(validity_type);
                String refillProId = rs.getString("REFILL_ID");
                comboMsisdn.setRefillProdId(refillProId);
                int dataDAId = rs.getInt("DA_ID");
                comboMsisdn.setDaId(dataDAId);

                list.add(comboMsisdn);
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
    public SegmentedrefillProdID getSegmentedrefillProdId(String refillProdId) {
        SegmentedrefillProdID refillProdIdCombo = null;
       
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder(); 

            sb.append("SELECT * "); 
            sb.append("FROM SEGMENTED_COMBO_OFFER_RID b, SEGMENTED_COMBO_MSISDN a "); 
            sb.append("where a.SEGMENT_ID = b.SEGMENT_ID  and b.REFILL_ID = '"+refillProdId+"' ");
            //sb.append("order by OPTION_ID asc ");           
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {
                refillProdIdCombo  = new SegmentedrefillProdID();
                

                int id = rs.getInt("ID");
                refillProdIdCombo.setId(id);
                String segmentedName = rs.getString("SEGMENTNAME");
                refillProdIdCombo.setSegName(segmentedName);
                String segment_Id = rs.getString("SEGMENT_ID");
                refillProdIdCombo.setSegment_Id(segment_Id);
                int type = rs.getInt("TYPE");
                refillProdIdCombo.setType(type);
                int price = rs.getInt("PRICE");
                refillProdIdCombo.setPrice(price);
                int benefit = rs.getInt("BENEFIT");
                refillProdIdCombo.setBenefit(benefit);
                int validity = rs.getInt("VALIDITY");
                refillProdIdCombo.setValidity(validity);
                String keyword = rs.getString("KEYWORD");
                refillProdIdCombo.setKeyword(keyword);
                String userOption = rs.getString("OPTION_ID");
                refillProdIdCombo.setUserOption(userOption);
                String unite = rs.getString("UNITE");
                refillProdIdCombo.setUnite(unite);
                String msisdnCombo = rs.getString("MSISDN");
                refillProdIdCombo.setMsisdnCombo(msisdnCombo);
                String validityType = rs.getString("VALIDITY_TYPE");
                refillProdIdCombo.setValidity_type(validityType);
                String refillProId = rs.getString("REFILL_ID");
                refillProdIdCombo.setRefillProdId(refillProId);
                int dataDAId = rs.getInt("DA_ID");
                refillProdIdCombo.setDaId(dataDAId);
                

            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return refillProdIdCombo;
    }
    
    
    
    public List<ComboBundle> getBundles(String msisdn) {
        List<ComboBundle> list = new ArrayList<ComboBundle>();

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();

            sb.append(" SELECT  * "); 
            sb.append(" FROM SEGMENTED_COMBO_OFFER_RID b,  SEGMENTED_COMBO_MSISDN a");
            sb.append(" where MSISDN= "+msisdn+" and a.SEGMENT_ID = b.SEGMENT_ID ");
            sb.append(" order by OPTION_ID asc ");
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);
            

            while( rs.next() )
            {
                ComboBundle dc = new ComboBundle();

                int id = rs.getInt("ID");
                dc.setId(id);
                String name = rs.getString("SEGMENTNAME");
                dc.setName(name);
                String segment_Id = rs.getString("SEGMENT_ID");
                dc.setSegment_Id(segment_Id);
                int type = rs.getInt("TYPE");
                dc.setType(type);
                int price = rs.getInt("PRICE");
                dc.setPrice(price);
                int benefit = rs.getInt("BENEFIT");
                dc.setBenefit(benefit);
                int validity = rs.getInt("VALIDITY");
                dc.setValidity(validity);
                String keyword = rs.getString("KEYWORD");
                dc.setKeyword(keyword);
                String userOption = rs.getString("OPTION_ID");
                dc.setUserOption(userOption);
                String unite = rs.getString("UNITE");
                dc.setUnite(unite);
                String msisdnCombo = rs.getString("MSISDN");
                dc.setMsisdnCombo(msisdnCombo);
                String validityType = rs.getString("VALIDITY_TYPE");
                dc.setValidity_type(validityType);
                String refillProId = rs.getString("REFILL_ID");
                dc.setRefillProdId(refillProId);
                int dataDAId = rs.getInt("DA_ID");
                dc.setDaId(dataDAId);

                list.add(dc);
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
    
    public ComboMsisdn getComboMSISDN(String msisdn) {
        ComboMsisdn resp = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("select * from SEGMENTED_COMBO_MSISDN where msisdn = '"+msisdn+"' ");
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {

                String  number = rs.getString("MSISDN");

                resp = new ComboMsisdn();
                resp.setMsisdn(number);
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }

}
