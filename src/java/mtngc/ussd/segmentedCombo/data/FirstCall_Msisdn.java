/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo.data;

import mtn.gn.com.data.combo.*;
import mtn.gn.com.data.*;
import java.io.Serializable;

/**
 *
 * @author EKhosa
 */
public class FirstCall_Msisdn implements Serializable {

    private String customerMsisdn;

    /**
     * @return the customerMsisdn
     */
    public String getCustomerMsisdn() {
        return customerMsisdn;
    }

    /**
     * @param customerMsisdn the customerMsisdn to set
     */
    public void setCustomerMsisdn(String customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
    }
    
    
}
