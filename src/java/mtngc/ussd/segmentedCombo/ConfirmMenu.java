/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.segmentedCombo;

import java.util.Enumeration;
import java.io.Serializable;
import java.util.*;
import mtngc.ussd.segmentedCombo.data.ComboBundle;
import mtngc.ussd.segmentedCombo.data.ComboData;

/**
 *
 * @author EKhosa
 */
public class ConfirmMenu {
    String req;
    private RequestEnum selection;
    
    static String PAR_CREDIT = "1";
    static String PAR_MOMO = "2";
    static String RETOUR = "#";

    public ConfirmMenu(RequestEnum selection){
       this.selection=selection;
        
    }
    public String getString(){
        StringBuilder sb = new StringBuilder(); 
        
        ComboData dataCombo = new ComboData();
        List<ComboBundle> list = dataCombo.getBundles(req);
        int i=1;
        for(ComboBundle bundle : list){
            sb.append("Confirmez vous l achat du forfait de "+bundle.getBenefit()+" FG unites valide 24h a "+bundle.getPrice()+" GNF?\n");
            i++;
        }
        sb.append("1. Confirm\n");
        sb.append("# Retour\n");
        //sb.append("2. Par MoMo\n");
        
        sb.append("Repondez");
         
        String str = sb.toString();
                
        return str;
    }
    
}
